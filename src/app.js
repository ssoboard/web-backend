const { createServer } = require("http");
const connect = require("./db");
const config = require("./config");
const app = require("./server");

const server = createServer(app);
connect();

server.listen(config.port, () => {
  console.log(`Server listening on port ${config.port}`);
});
