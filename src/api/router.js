const express = require("express");
const authRouter = require("./resources/auth");

const router = express.Router();

// Auth routes
router.use("/auth", authRouter);

// Router

// Result
// Cookies:  [Object: null prototype] {}
// Signed Cookies:  [Object: null prototype] {}

module.exports = router;
