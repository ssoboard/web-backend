const { body: validateBody, validationResult } = require("express-validator");

const validateHandler = function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log("밸리데이션 에러", errors);

    return res.status(400).send({
      message: errors.array()
    });
  }
  next();
};

const validate = {
  json: validateBody()
    .exists()
    .withMessage("Body must be valid JSON"),
  email: validateBody("email")
    .exists()
    .withMessage("Body must contain `email`")
    .bail()
    .isEmail()
    .withMessage("Body must have valid email"),
  password: validateBody("password")
    .exists()
    .withMessage("Body must contain `password`")
    .bail()
    .custom(value => {
      const regexPassword = new RegExp(
        "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z])(?=.*[!@#\\$%\\^&\\*]).{8,}$"
      );
      const isValidPassword = regexPassword.test(value);
      console.log(isValidPassword);
      if (!isValidPassword) {
        return Promise.reject(
          "Body must have valid password (At Least 1 Upper Case, 1 lower case, 1 spacial character, 1 numeric character)"
        );
      } else {
        return true;
      }
    }),
  hello: ""
};

module.exports = {
  validateHandler,
  validate
};
