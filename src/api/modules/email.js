const nodemailer = require("nodemailer");

const config = require("../../config");
const User = require("../resources/user/user.model");

let mailConfig;
if (config.nodeEnv === "production") {
  mailConfig = {
    service: "gmail",
    host: "smtp.gmail.com",
    port: 587,
    secure: false,
    auth: {
      user: config.auth.gmailUser,
      pass: config.auth.gmailPassword
    }
  };
}

const transporter = nodemailer.createTransport(mailConfig);

const passwordResetURL = token => {
  return `localhost:3000/reset_password/${token}`;
};

const passwordResetTemplate = (user, passwordResetURL) => {
  const from = config.auth.gmailUser;
  const to = user.email;
  const subject = "Password Reset";
  const html = `
  This link should take the user to a Password Form but you can try this link in Postman. 
  Required body data: "password" ... ${passwordResetURL}
  `;

  return { from, to, subject, html };
};

const signUpConfirmationURL = token => {
  return `localhost:3000/auth/confirmation/${token}`;
};

const signUpConfirmationTemplate = (user, emailConfirmationUrl) => {
  const from = "Horong2020@gmail.com";
  const to = user.email;
  const subject = "Welcome";
  const html = `
  Thanks for signing up to S.Board!
  To confirm your account click here: <Link href="${emailConfirmationUrl}"><a>${emailConfirmationUrl}</a></Link>
  `;

  return { from, to, subject, html };
};

const sendSignUpConfirmationEmail = async email => {
  try {
    let user = await User.findOne({ email });

    if (user) {
      await user.verifyEmail();
      await user.save();

      const token = user.email_verify_token;
      const url = signUpConfirmationURL(token);
      const emailTemplate = signUpConfirmationTemplate(user, url);

      try {
        let info = await transporter.sendMail(emailTemplate);
        console.log("Preview: " + nodemailer.getTestMessageUrl(info));
      } catch (err) {
        console.log(err);
      }
    }
  } catch (err) {
    console.log(err);
  }
};

const receiveNewPassword = () => {};

module.exports = {
  transporter,
  passwordResetURL,
  passwordResetTemplate,
  sendSignUpConfirmationEmail,
  receiveNewPassword
};
