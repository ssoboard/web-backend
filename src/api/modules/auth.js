const jwt = require("jsonwebtoken");
const User = require("../resources/user/user.model");
const moment = require("moment");
const config = require("../../config");

/**
 * Auth Module
 * - protect : 토큰 검증
 * - getUserFromToken : 토큰으로부터 사용자 정보 추출
 * - getUserIdByRefreshToken : 리프레시 토큰에서 아이디 추출
 */
/* THIS FUNCTION IS USED TO CHECK IF THE USER IS LOGGED IN, THEIR TOKENS DID NOT EXPIRE AND THE SESSION STRING IS EQUAL. TO BE USED BEFORE SHOWING PROTECTED CONTENT.
    ALSO REFRESHES ACCESS TOKEN AND REFRESH TOKEN. */
const clearToken = (res, statusCode) => {
  return res
    .clearCookie("access_token")
    .clearCookie("refresh_token")
    .sendStatus(statusCode);
};
const verifyRefreshToken = (req, res, next, refresh_token) => {
  jwt.verify(
    refresh_token,
    config.auth.jwtRefreshSecret,
    async (err, decoded) => {
      if (err) return clearToken(res, 401);

      if (decoded) {
        // check if token is expired
        let tempdate = moment(decoded.exp * 1000);
        if (!tempdate.isAfter(moment())) return clearToken(res, 403);

        // check if token does not match sessionString
        let userToVerify = await User.findOne({ _id: decoded.id });
        if (!(userToVerify.session_string === decoded.session_string))
          return clearToken(res, 401);

        // If valid, issue new tokens to the client
        const newAccessToken = jwt.sign(
          { id: decoded.id, session_string: decoded.session_string },
          config.auth.jwtSecret,
          {
            expiresIn: "5m"
          }
        );

        const newRefreshToken = jwt.sign(
          { id: decoded.id, session_string: decoded.session_string },
          config.auth.jwtRefreshSecret,
          {
            expiresIn: "30d"
          }
        );

        req.payload = decoded;

        res
          .cookie("access_token", newAccessToken, {
            maxAge: 300000,
            httpOnly: true,
            signed: true,
            secret: config.auth.cookieSecret,
            secure: config.nodeEnv === "production" ? true : false
          })
          .cookie("refresh_token", newRefreshToken, {
            maxAge: 2592000000,
            httpOnly: true,
            signed: true,
            secret: config.auth.cookieSecret,
            secure: config.nodeEnv === "production" ? true : false
          });

        return next();
      }
    }
  );
};
const protect = (req, res, next) => {
  try {
    if (req.body.password_reset_token) return next();

    console.log(req.signedCookies);

    if (!req.signedCookies.access_token) {
      if (!req.signedCookies.refresh_token) {
        return clearToken(res, 400);
      }
    }

    let access_token = "";
    if (req.signedCookies.access_token) {
      access_token = req.signedCookies.access_token;
      access_token = access_token.replace("Bearer ", "");
    }

    let refresh_token = req.signedCookies.refresh_token;
    refresh_token = refresh_token.replace("Bearer ", "");

    jwt.verify(access_token, config.auth.jwtSecret, (err, decoded) => {
      if (err) {
        verifyRefreshToken(req, res, next, refresh_token);
      }

      if (decoded) {
        let tempdate = moment(decoded.exp * 1000);
        if (tempdate.isAfter(moment())) {
          // Check if refresh token is not expired, else kick him out
          if (req.signedCookies.refresh_token) {
            verifyRefreshToken(req, res, next, refresh_token);
          } else {
            return clearToken(res, 403);
          }
        } else {
          return clearToken(res, 401);
        }
      }
    });
  } catch (err) {
    return clearToken(res, 500);
  }
};

// const getUserFromToken = async function(req, res, next) {};
// const getUserIdByRefreshToken = async function(refreshToken) {};
module.exports = {
  protect,
  clearToken
};
