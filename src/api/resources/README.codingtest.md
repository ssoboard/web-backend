# CodingTest
## codingtest_question
``` javascript
{
  _id: ObjectId("563479cc8a8a4246bd27d784"),
  title:'최대값 구하기',
  content:`
  <div class="question">
    <p>
      <span>ANIMAL_INS</span> 테이블은 동물 보호소에 들어온 동물의 정보를 담은 테이블입니다. <span>ANIMAL_INS</span> 테이블 구조는 다음과 같으며, <span>ANIMAL_ID</span>, <span>ANIMAL_TYPE</span>, <span>DATETIME</span>, <span>INTAKE_CONDITION</span>, <span>NAME</span>, <span>SEX_UPON_INTAKE</span>는 각각 동물의 아이디, 생물 종, 보호 시작일, 보호 시작 시 상태, 이름, 성별 및 중성화 여부를 나타냅니다.
    </p>
    <table class="table">
      <thead>
      <tr>
        <th>NAME</th>
        <th>TYPE</th>
        <th>NULLABLE</th>
      </tr>
      </thead>
      <tbody>
        <tr>
          <td>ANIMAL_ID</td>
          <td>VARCHAR(N)</td>
          <td>FALSE</td>
        </tr>
        <tr>
          <td>ANIMAL_TYPE</td>
          <td>VARCHAR(N)</td>
          <td>FALSE</td>
        </tr>
        <tr>
          <td>DATETIME</td>
          <td>DATETIME</td>
          <td>FALSE</td>
        </tr>
        <tr>
          <td>INTAKE_CONDITION</td>
          <td>VARCHAR(N)</td>
          <td>FALSE</td>
        </tr>
        <tr>
          <td>NAME</td>
          <td>VARCHAR(N)</td>
          <td>TRUE</td>
        </tr>
        <tr>
          <td>SEX_UPON_INTAKE</td>
          <td>VARCHAR(N)</td>
          <td>FALSE</td>
        </tr>
      </tbody>
    </table>
    <p>가장 최근에 들어온 동물은 언제 들어왔는지 조회하는 SQL 문을 작성해주세요. </p>

    <!-- 예시 -->
    <h5>예시</h5>
    <p>예를 들어 <span>ANIMAL_INS</span> 테이블이 다음과 같다면</p>
    <table class="table">
      <thead>
        <tr>
          <th>ANIMAL_ID</th>
          <th>ANIMAL_TYPE</th>
          <th>DATETIME</th>
          <th>INTAKE_CONDITION</th>
          <th>NAME</th>
          <th>SEX_UPON_INTAKE</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>A399552</td>
          <td>Dog</td>
          <td>2013-10-14 15:38:00</td>
          <td>Normal</td>
          <td>Jack</td>
          <td>Neutered Male</td>
        </tr>
        <tr>
          <td>A379998</td>
          <td>Dog</td>
          <td>2013-10-23 11:42:00</td>
          <td>Normal</td>
          <td>Disciple</td>
          <td>Intact Male</td>
        </tr>
        <tr>
          <td>A370852</td>
          <td>Dog</td>
          <td>2013-11-03 15:04:00</td>
          <td>Normal</td>
          <td>Katie</td>
          <td>Spayed Female</td>
        </tr>
        <tr>
          <td>A403564</td>
          <td>Dog</td>
          <td>2013-11-18 17:03:00</td>
          <td>Normal</td>
          <td>Anna</td>
          <td>Spayed Female</td>
        </tr>
      </tbody>
    </table>

    <!-- 결과 -->
    <p>가장 늦게 들어온 동물은 Anna이고, Anna는 2013-11-18 17:03:00에 들어왔습니다. 따라서 SQL문을 실행하면 다음과 같이 나와야 합니다.</p>
    <table class="table">
      <thead>
        <tr>
          <th>시간</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>2013-11-18 17:03:00</td>
        </tr>
      </tbody>
    </table>
    <hr>

    <!-- 저작권 정보 -->
    <p>본 문제는 <a href="https://www.kaggle.com/aaronschlegel/austin-animal-center-shelter-intakes-and-outcomes" target="_blank" rel="noopener">Kaggle의 <q>Austin Animal Center Shelter Intakes and Outcomes</q></a>에서 제공하는 데이터를 사용하였으며 <a href="https://opendatacommons.org/licenses/odbl/1.0/" target="_blank" rel="noopener">ODbL</a>의 적용을 받습니다.</p>
  </div>
  `,
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```
------
## codingtest_answer
``` javascript
{
  user_id: ObjectId("563479cc8a8a4246bd27d784"),
  
  _id: ObjectId("563479cc8a8a4246bd27d784"),
  content:
  `function solution() {
    hello hello
  }`,
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```
------