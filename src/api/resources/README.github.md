# Github
## github
``` javascript
{
  api_id: 1,
  
  _id: ObjectId("563479cc8a8a4246bd27d784"),
  url: "https://github.com/as-you-say/",
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```
------
## github_repository
``` javascript
{
  api_id: 1,
  github_id: ObjectId("563479cc8a8a4246bd27d784"),

  _id: ObjectId("563479cc8a8a4246bd27d784"),
  url: "https://github.com/as-you-say/:repo",
  name: "React Template",
  description: "React Template Example - OAuth2/Local sign in and sign up",
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```
------
## github_branch
``` javascript
{
  api_id: 1,
  repository_id: ObjectId("563479cc8a8a4246bd27d784"),

  _id: ObjectId("563479cc8a8a4246bd27d784"),
  name: "master",
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```
------
## github_issue
``` javascript
{
  api_id: 1,
  repository_id: ObjectId("563479cc8a8a4246bd27d784"),

  _id: ObjectId("563479cc8a8a4246bd27d784"),
  title:"Error !!",
  description:"Login function error",
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```
------
## github_pull_request
``` javascript
{
  api_id: 1,
  repository_id: ObjectId("563479cc8a8a4246bd27d784"),

  _id: ObjectId("563479cc8a8a4246bd27d784"),
  title:"Pull Request - hey ~ hey ~",
  description:"Modified login page",
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```
------