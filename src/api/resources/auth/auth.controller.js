const nodemailer = require("nodemailer");
const jwt = require("jsonwebtoken");
const { randomBytes } = require("crypto");
const config = require("../../../config");

const { validateHandler, validate } = require("../../modules/validate");
const User = require("../user/user.model");
const {
  transporter,
  passwordResetURL,
  passwordResetTemplate,
  sendSignUpConfirmationEmail
} = require("../../modules/email");
const { clearToken } = require("../../modules/auth");

const MESSAGE = {
  ALREADY_USER: "User already exists.",
  ALREADY_LOGGED_IN: "You're already logged in!",
  CONFIRM_EMAIL: "Success! Please check your e-mail to confirm your account",
  TRY_AGAIN:
    "Oops! Our server encountered an error during your request. Please try again!",
  NOT_MATCHED: "Your username and password don't match.",
  LOGGED_OUT: "Successfully logged out",
  A: ""
};

/**
 * Single-Factor Authentication
 * - signUp : 회원가임
 * - signIn : 로그인
 * - signOut : 로그아웃
 */
const signUp = [
  validate.json,
  validate.email,
  validate.password,
  validateHandler,
  async (req, res) => {
    // 회원가입 정보
    const { email, password, ...rest } = req.body;

    try {
      // 리프레시 토큰 존재여부 검증
      if (req.signedCookies.refresh_token)
        return res.sendStatus(400).send(MESSAGE.ALREADY_LOGGED_IN); // 리프레시 토큰 - 로그인되어있는데 회원가입 요청하면 400 에러

      // 동일한 이메일 존재여부 검증
      let user = await User.findOne({ email });
      if (user) return res.status(409).send(MESSAGE.ALREADY_USER); // 이메일이 이미 있다면 409 에러

      // 세션 아이디를 추가 후, 사용자 생성
      let session_string = randomBytes(32).toString("hex"); // 랜덤문자열 추가 후 사용자 생성
      let newUser = new User({ email, password, session_string, ...rest });

      // 사용자 생성 후 이메일 전송
      try {
        await newUser.save();
        sendSignUpConfirmationEmail(email); // 이메일로 계정 활성화 링크 전송
        res.status(201).send(MESSAGE.CONFIRM_EMAIL); // 이메일 확인하세요~ 메세지 보여줌
      } catch (err) {
        const _message = err.message; // log this to somewhere
        console.log(_message);
        res.status(500).send(MESSAGE.TRY_AGAIN); // 사용자 생성 및 이메일 전송시 에러나면 재요청 바람
      }
    } catch (err) {
      return res.status(500).send(MESSAGE.TRY_AGAIN); // 그 외에 에러나면 재요청 바람
    }
  }
];

const confirmation = [
  async (req, res) => {
    const { token } = req.params;
    console.log("confirmation 요청");

    if (token) {
      let email_verify_token = token;

      /* The following code checks if the user exists, if the user's email is not already verified and if the server has any trouble validating the request.
          If all goes well, the email on the user gets verified and the email verify token gets reset. */
      try {
        const user = await User.findOne({ email_verify_token });
        if (!user) return res.sendStatus(409);

        if (email_verify_token) {
          user.email_verified = true;
          user.email_verify_token = undefined;
          try {
            user.save();
            return res.sendStatus(200);
          } catch (err) {
            return res.sendStatus(500);
          }
        }
      } catch (err) {
        return res.sendStatus(500);
      }
    } else {
      return res.sendStatus(401);
    }
  }
];

const signIn = [
  validate.json,
  validate.email,
  validate.password,
  validateHandler,
  async (req, res) => {
    const { email, password } = req.body;

    /* The following code checks if the user is already logged in,
        if the passwords match, or if the user requires 2FA, or the server somehow has errors logging the user in.
        If the user requires 2FA, they will receive a temporary token (5 minutes) from which they will
        only need to input their 2FA code to login. Else, they get logged in the platform. */
    try {
      if (req.signedCookies.refresh_token)
        return res.status(400).send(MESSAGE.ALREADY_LOGGED_IN);

      let oldUser = await User.findOneAndUpdate(
        { email },
        { last_login: Date.now(), $inc: { login_count: 1 } },
        { new: false }
      ).exec();

      console.log(oldUser);
      if (!oldUser) return res.sendStatus(401);
      oldUser.comparePassword(password, (err, isMatch) => {
        if (err) res.status(500).send(MESSAGE.TRY_AGAIN);
        if (!isMatch) return res.status(401).send(MESSAGE.NOT_MATCHED);

        const access_token = jwt.sign(
          { id: oldUser._id, session_string: oldUser.session_string },
          config.auth.jwtSecret,
          { expiresIn: "5m" }
        );
        const refresh_token = jwt.sign(
          { id: oldUser._id, session_string: oldUser.session_string },
          config.auth.jwtRefreshSecret,
          { expiresIn: "30d" }
        );

        // token (access token) lasts for 5 minutes
        // refreshToken lasts for 6 months
        res
          .cookie("access_token", access_token, {
            maxAge: 300000,
            httpOnly: true,
            signed: true,
            secret: config.auth.cookieSecret,
            secure: false
            // secure: config.nodeEnv === "production" ? true : false
          })
          .cookie("refresh_token", refresh_token, {
            maxAge: 2592000000,
            httpOnly: true,
            signed: true,
            secret: config.auth.cookieSecret,
            secure: false
            // secure: config.nodeEnv === "production" ? true : false
          })
          .status(200)
          .send("OK");
      });
    } catch (err) {
      res.sendStatus(500).send(err);
    }
  }
];

const signOut = [
  async (req, res) => {
    clearToken(res, 200).send(MESSAGE.LOGGED_OUT);
  }
];

const resetPasswordAndSendMail = [
  validate.json,
  validate.email,
  validateHandler,
  async (req, res) => {
    const { email } = req.body;

    try {
      let user = await User.findOne({ email });

      if (user) {
        await user.resetPassword();
        await user.save();

        const token = user.password_reset_token;
        const url = passwordResetURL(token);
        const emailTemplate = passwordResetTemplate(user, url);

        try {
          let info = await transporter.sendMail(emailTemplate);
          console.log("Preview: " + nodemailer.getTestMessageUrl(info));
        } catch (err) {
          console.log(err);
        }
      }
    } catch (err) {
      console.log(err);
    }

    res.sendStatus(200);
  }
];

const changePassword = [
  validate.json,
  validate.password,
  validateHandler,
  async (req, res) => {
    const { token } = req.params;
    const { password } = req.body;

    try {
      const user = await User.findOne({ password_reset_token: token });

      if (user) {
        // Check if the token hasn't expired yet
        try {
          if (user.password_reset_token_expiry) {
            let tempdate = new Date();
            let tempdbdate = new Date(user.password_reset_token_expiry);

            if (tempdate < tempdbdate) {
              try {
                //await User.findOneAndUpdate({ resetToken:token }, { password: hash });
                user.password = password;
                user.password_reset_token = undefined;
                user.password_reset_token_expiry = undefined;
                user.session_string = randomBytes(32).toString("hex");
                user.save();
                res.sendStatus(200);
              } catch (err) {
                console.log(err);
              }
            } else res.sendStatus(410);
          } else {
            res.sendStatus(410);
          }
        } catch (err) {
          console.log(err);
          res.sendStatus(410);
        }
      } else res.sendStatus(410);
    } catch (err) {
      res.sendStatus(404);
    }
  }
];

/**
 * Two-Factor Authentication
 * - tfaSignIn : tfa 로그인
 * - tfaRequest : tfa 요청하기
 * - tfaSend : tfa 코드 보내기
 */
// const tfaSignIn = async function(req, res, next) {};
// const tfaRequest = async function(req, res, next) {};
// const tfaSend = async function(req, res, next) {};

module.exports = {
  signUp,
  confirmation,
  signIn,
  signOut,
  resetPasswordAndSendMail,
  changePassword
};
