const express = require("express");
const authController = require("./auth.controller");
const { protect } = require("../../modules/auth");

const authRouter = express.Router();

// 회원가입
authRouter.route("/signup").post(authController.signUp);

// 회원가입 메일 확인
authRouter.route("/confirmation/:token").get(authController.confirmation);

// 로그인
authRouter.route("/signin").post(authController.signIn);

// 로그아웃
authRouter.route("/signout").get(protect, authController.signOut);

// 비밀번호 초기화 후 초기화 링크를 메일로 전송 (링크는 프론트엔드 애플리케이션의 페이지)
authRouter
  .route("/reset_password")
  .post(authController.resetPasswordAndSendMail);

// 프론트엔드 애플리케이션에서 인증서비스에 post 방식으로 비밀번호 변경 요청 (토큰 + 변경할 비밀번호 + 아이디)
authRouter
  .route("/change_password/:token")
  .post(protect, authController.changePassword);

module.exports = authRouter;
