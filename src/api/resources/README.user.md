# User

## user
``` javascript
{
  _id: ObjectId("563479cc8a8a4246bd27d784"),
  name: 'Horong'
  email: "horong@gmail.com"
  password:"d1239eklhagklhdajk1234789dfjkaghjk324891345789rfgjkhsfd",
  password_update_at: new Date().getTime(),
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```
## user_token
``` javascript
{
  user_id: ObjectId("563479cc8a8a4246bd27d784"),
  _id: ObjectId("563479cc8a8a4246bd27d784"),
  type: 'github',
  access_token: 'd1239eklhagklhdajk1234789dfjkaghjk324891345789rfgjkhsfd',
  refresh_token: 'd1239eklhagklhdajk1234789',
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```