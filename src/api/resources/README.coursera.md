# Coursera
## coursera
``` javascript
{
  _id: ObjectId("563479cc8a8a4246bd27d784"),
  university: "university of minnesota",
  title: "Earn your UX design certificate in just 6 months",
  description: "Apply design theories to your next project with the UX Design MasterTrack Certificate.",
  url:'https://www.coursera.org/',
  total: 10,
  complete: 7,
  tages: ['json', 'xml', 'SQL'],
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```
------
## coursera_lecture
``` javascript
{
  _id: ObjectId("563479cc8a8a4246bd27d784"),
  order: 1,
  title: 'Lecture - 1 : Hello World!',
  memo: `
  Welcome to Lesson | Small Talk & Conversational Vocabulary!
  You’re joining thousands of learners currently enrolled in the 
  course. I'm excited to have you in the class and look forward to 
  your contributions to the learning community.

  To begin, I recommend taking a few minutes to explore the course 
  site. Review the material we’ll cover each week, and preview the 
  assignments you’ll need to complete to pass the course. Click 
  Discussions to see forums where you can discuss the course 
  material with fellow students taking the class.

  If you have questions about course content, please post them in 
  the forums to get help from others in the course community. For 
  technical problems with the Coursera platform, visit the Learner 
  Help Center.

  Good luck as you get started, and I hope you enjoy the course!
  `,
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```
------