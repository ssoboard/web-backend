const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const { randomBytes } = require("crypto");
const config = require("../../../config");

// Authy API (2FA)
const authy = require("authy")(config.auth.tfaApiAuthykey);

const Schema = mongoose.Schema;
const schema = {
  // Email
  email: {
    required: [true, "Email is required."],
    type: String,
    unique: true
  },
  email_verified: {
    required: false,
    type: Boolean
  },
  email_verify_token: {
    required: false,
    type: String,
    unique: true
  },

  // Phone
  country_code: String,
  phone: {
    required: false,
    type: String
    // unique: true
  },
  phone_verified: {
    required: false,
    type: Boolean
  },
  phone_verify_token: {
    required: false,
    type: String
    // unique: false
  },

  // Password
  password: {
    required: [true, "Password is required."],
    type: String
  },
  password_reset_token: {
    required: false,
    type: String
  },
  password_reset_token_expiry: {
    required: false,
    type: Date
  },

  // Authy ID
  authy_id: String,

  // Access Token
  access_token: {
    required: false,
    type: String
  },

  // Session Key
  session_string: {
    required: true,
    type: String
  },

  last_login: {
    type: Date,
    default: Date.now()
  },
  login_count: {
    required: false,
    type: Number,
    default: 0
  }
};

const userSchema = new Schema(schema, { timestamps: true });

userSchema.pre("save", function(next) {
  const user = this;
  if (!user.isModified("password")) return next();

  try {
    bcrypt.hash(user.password, 10, function(err, hash) {
      if (err) return next(err);
      user.password = hash;
      next();
    });
  } catch (err) {
    console.log(err);
  }
});

userSchema.methods.comparePassword = function(candidatePassword, cb) {
  if (this.password_reset_token !== undefined) {
    return cb(null, false);
  }

  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

// Send a verification token to this user (2FA)
userSchema.methods.sendAuthyToken = function(phone, countryCode, cb) {
  var self = this;

  if (!self.authy_id) {
    // Register this user if it's a new user
    authy.register_user(self.email, phone, countryCode, function(
      err,
      response
    ) {
      if (err || !response.user) return cb.call(self, err);
      self.authy_id = response.user.id;
      self.save(function(err, doc) {
        if (err || !doc) return cb.call(self, err);
        self = doc;
        sendToken();
      });
    });
  } else {
    // Otherwise send token to a known user
    sendToken();
  }

  // With a valid Authy ID, send the 2FA token for this user
  function sendToken() {
    authy.request_sms(self.authy_id, true, function(err) {
      cb.call(self, err);
    });
  }
};

// Verify function (2FA)
userSchema.methods.verifyAuthyToken = function(otp, cb) {
  const self = this;
  authy.verify(self.authy_id, otp, function(err, response) {
    cb.call(self, err, response);
  });
};

userSchema.methods.resetPassword = function() {
  return new Promise((res, rej) => {
    randomBytes(32, (err, buf) => {
      if (err) {
        const err = new Error("Internal error");
        err.status = 500;
        rej(err);
      }

      const generateToken = () => {
        const token = buf.toString("hex");
        User.findOne({ password_reset_token: token }, (err, user) => {
          if (err) {
            const err = new Error("Internal error");
            err.status = 500;
            rej(err);
          }

          if (user) return generateToken();

          this.password_reset_token = token;
          let tempdate = new Date();
          tempdate.setHours(tempdate.getHours() + 24);
          this.password_reset_token_expiry = tempdate.toISOString();
          res();
        });
      };

      generateToken();
    });
  });
};

userSchema.methods.verifyEmail = function() {
  return new Promise((res, rej) => {
    randomBytes(32, (err, buf) => {
      if (err) {
        const err = new Error("Internal error");
        err.status = 500;
        rej(err);
      }

      const generateToken = () => {
        const token = buf.toString("hex");
        User.findOne({ email_verify_token: token }, (err, user) => {
          if (err) {
            const err = new Error("Internal error");
            err.status = 500;
            rej(err);
          }

          if (user) return generateToken();

          this.email_verify_token = token;
          res();
        });
      };
      generateToken();
    });
  });
};

userSchema.set("toJSON", {
  transform: function(doc, ret) {
    let retJSON = {
      email: ret.email,
      email_verified: ret.email_verified,
      email_verify_token: ret.email_verify_token,
      id: ret._id,
      last_login: ret.last_login,
      login_count: ret.login_count,
      access_token: ret.access_token,
      password_reset_token: ret.resetToken,
      password_reset_token_expiry: ret.password_reset_token_expiry
    };

    return retJSON;
  }
});

mongoose.set("useCreateIndex", true);
mongoose.set("useFindAndModify", false);
const User = mongoose.model("User", userSchema, "users");

module.exports = User;
