# Gitlab
## gitlab
``` javascript
{
  api_id: 1,
  
  _id: ObjectId("563479cc8a8a4246bd27d784"),
  url: "https://gitlab.com/as-you-say/",
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```
------
## gitlab_repository
``` javascript
{
  api_id: 1,
  gitlab_id: ObjectId("563479cc8a8a4246bd27d784"),

  _id: ObjectId("563479cc8a8a4246bd27d784"),
  url: "https://gitlab.com/as-you-say/:repo",
  name: "React Template",
  description: "React Template Example - OAuth2/Local sign in and sign up",
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```
------
## gitlab_branch
``` javascript
{
  api_id: 1,
  repository_id: ObjectId("563479cc8a8a4246bd27d784"),

  _id: ObjectId("563479cc8a8a4246bd27d784"),
  name: "master",
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```
------
## gitlab_issue
``` javascript
{
  api_id: 1,
  repository_id: ObjectId("563479cc8a8a4246bd27d784"),

  _id: ObjectId("563479cc8a8a4246bd27d784"),
  title:"Error !!",
  description:"Login function error",
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```
------
## gitlab_merge_request
``` javascript
{
  api_id: 1,
  repository_id: ObjectId("563479cc8a8a4246bd27d784"),

  _id: ObjectId("563479cc8a8a4246bd27d784"),
  title:"Merge Request - hey ~ hey ~",
  description:"Modified login page",
  create_at: new Date().getTime(),
  update_at: new Date().getTime(),
}
```
------