const { router } = require("./api");
const express = require("express");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const cors = require("cors");
const errorhandler = require("errorhandler");

const config = require("./config");
const isProduction = process.env.NODE_ENV === "production";
const app = express();

app.use(
  cors({
    origin: true,
    credentials: true
  })
);

app.use(cookieParser(config.auth.cookieSecret)); // 쿠키 파서

app.use(require("morgan")("dev"));
app.use(bodyParser.urlencoded({ extended: true })); // application/x-www-form-urlencoded
app.use(bodyParser.json()); // application/json

app.use(require("method-override")());

if (isProduction) {
  app.use(errorhandler());
}

// 라우터
app.use("/", router);

// 테스트 : 쿠키 체크
app.get("/cookieCheck", req => {
  console.log(req.signedCookies);
});

// Catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error("Not found");
  err.status = 404;
  next(err);
});

app.use((err, _req, res, _next) => {
  if (!isProduction) {
    console.log(err);
  }

  res.status(err.status || 500).json({
    errors: Array.isArray(err.message) ? err.message : { message: err.message }
  });
});

module.exports = app;
