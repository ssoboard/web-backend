const dotenv = require("dotenv");
dotenv.config();

const config = {
  // nodeEnv: process.env.NODE_ENV,
  nodeEnv: "production",
  port: process.env.PORT || 3300,
  db: {
    url: process.env.SSO_DATABASE_URL
  },
  sso: {
    github: {
      clientId: process.env.SSO_GITHUB_CLIENT_ID,
      clientSecret: process.env.SSO_GITHUB_CLIENT_SECRET,
      codeRedirectUrl: process.env.SSO_GITHUB_CODE_REDIRECT_URL,
      tokenRedirectUrl: process.env.SSO_GITHUB_TOKEN_REDIRECT_URL
    },
    gitlab: {
      clientId: process.env.SSO_GITLAB_CLIENT_ID,
      clientSecret: process.env.SSO_GITLAB_CLIENT_SECRET,
      codeRedirectUrl: process.env.SSO_GITLAB_CODE_REDIRECT_URL,
      tokenRedirectUrl: process.env.SSO_GITLAB_TOKEN_REDIRECT_URL
    }
  },
  auth: {
    jwtSecret: process.env.SSO_JWT_SECRET,
    jwtRefreshSecret: process.env.SSO_JWT_REFRESH_SECRET,
    cookieSecret: process.env.SSO_COOKIE_SECRET,
    tfaApiAuthykey: process.env.SSO_TFA_API_AUTHYKEY,
    gmailUser: process.env.SSO_GMAIL_EMAIL_USER,
    gmailPassword: process.env.SSO_GMAIL_EMAIL_PASSWORD
  }
};

module.exports = config;
